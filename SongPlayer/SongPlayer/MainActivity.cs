﻿using System;
using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Android.Media;
using static Android.Views.View;

namespace SongPlayer
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        MediaPlayer _player;
        Button playButton;
        Button stopButton;
        
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_main);
            _player = MediaPlayer.Create(this, Resource.Raw.A1);


            playButton = FindViewById<Button>(Resource.Id.PlayPauseButtonID);
            playButton.Click += (sender, e) =>
            {
                if (_player.IsPlaying)
                {
                    playButton.Text = "Paused";
                    _player.Pause();
                }else
                {
                    playButton.Text = "Playing";
                    _player.Start();
                }
            };


            stopButton = FindViewById<Button>(Resource.Id.StopButtonID);
            stopButton.Click += (sender, e) =>
             {
                 if (_player.IsPlaying)
                 {
                     stopButton.Text = "Stopped";
                     playButton.Text = "Play";
                     _player.Stop();
                 }
             };




            var nextButton = FindViewById<Button>(Resource.Id.NextButtonID);
            nextButton.Click += (sender, e) =>
            {
                CommonMethod();
            };



            var NextJumpButton = FindViewById<Button>(Resource.Id.NextJumpButtonID);
            NextJumpButton.Click += (sender, e) =>
            {
                CommonMethod();
            };


            var PreviousJumpButton = FindViewById<Button>(Resource.Id.PJumpButtonID);
            PreviousJumpButton.Click += (sender, e) =>
            {
                CommonMethod();
            };

            TableRow rowOne = FindViewById<TableRow>(Resource.Id.one);
            rowOne.Click += (sender, e) => {
                if (_player.IsPlaying)
                {
                    _player.Stop();
                }else { 
                _player = MediaPlayer.Create(this, Resource.Raw.A1);
                _player.Start();
                 stopButton.Text = "stop";

                }

            };

            TableRow rowTwo= FindViewById<TableRow>(Resource.Id.two);
            rowTwo.Click += (sender, e) => {
                if (_player.IsPlaying)
                {
                    _player.Stop();
                }
                else { 
                _player = MediaPlayer.Create(this, Resource.Raw.A2);
                _player.Start();
                stopButton.Text = "stop";
                }

            };


            TableRow rowThree = FindViewById<TableRow>(Resource.Id.three);
            rowThree.Click += (sender, e) => {
                if (_player.IsPlaying)
                {
                    _player.Stop();
                }
                else
                {
                    _player = MediaPlayer.Create(this, Resource.Raw.A3);
                    _player.Start();
                    stopButton.Text = "stop";
                }

            };

            TableRow rowFour = FindViewById<TableRow>(Resource.Id.four);
            rowFour.Click += (sender, e) => {
                if (_player.IsPlaying)
                {
                    _player.Stop();
                }
                else
                {
                    _player = MediaPlayer.Create(this, Resource.Raw.A4);
                    _player.Start();
                    stopButton.Text = "stop";
                }

            };


            TableRow rowFive = FindViewById<TableRow>(Resource.Id.five);
            rowFive.Click += (sender, e) => {
                if (_player.IsPlaying)
                {
                    _player.Stop();
                }
                else
                {
                    _player = MediaPlayer.Create(this, Resource.Raw.A5);
                    _player.Start();
                    stopButton.Text = "stop";
                }

            };

            Android.Support.V7.Widget.Toolbar toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            FloatingActionButton fab = FindViewById<FloatingActionButton>(Resource.Id.fab);
            fab.Click += FabOnClick;
        }

        private void CommonMethod()
        {
            Random mRand = new Random();
            int x = mRand.Next(5);
            switch (x)
            {
                case 1:
                    if (_player.IsPlaying)
                    {
                        _player.Stop();
                    }
                    _player = MediaPlayer.Create(this, Resource.Raw.A1);
                    _player.Start();
                    playButton.Text = "Playing";
                    break;
                case 2:
                    if (_player.IsPlaying)
                    {
                        _player.Stop();
                    }
                    _player = MediaPlayer.Create(this, Resource.Raw.A2);
                    _player.Start();
                    playButton.Text = "Playing";

                    break;
                case 3:
                    if (_player.IsPlaying)
                    {
                        _player.Stop();
                    }
                    _player = MediaPlayer.Create(this, Resource.Raw.A3);
                    _player.Start();
                    playButton.Text = "Playing";

                    break;
                case 4:
                    if (_player.IsPlaying)
                    {
                        _player.Stop();
                    }
                    _player = MediaPlayer.Create(this, Resource.Raw.A4);
                    _player.Start();
                    playButton.Text = "Playing";

                    break;
                case 5:
                    if (_player.IsPlaying)
                    {
                        _player.Stop();
                    }
                    _player = MediaPlayer.Create(this, Resource.Raw.A5);
                    _player.Start();
                    playButton.Text = "Playing";

                    break;
               
            }
        }


        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_settings)
            {
                return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        private void FabOnClick(object sender, EventArgs eventArgs)
        {
            View view = (View)sender;
            Snackbar.Make(view, "Replace with your own action", Snackbar.LengthLong)
                .SetAction("Action", (Android.Views.View.IOnClickListener)null).Show();
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}

